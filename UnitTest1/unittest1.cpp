#include "stdafx.h"
#include "Math.h"
#include "CppUnitTest.h"
#include "../ConsoleApplication2/ConsoleApplication2.cpp"


using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace ConsoleApplication_Test
{
	TEST_CLASS(UnitTest1)
	{
	public:

		TEST_METHOD(palindrom_test)
		{
			char c[] = "123321";
			Assert::AreEqual(true, Palindrom(c));
		}

		TEST_METHOD(nepalindrom_test)
		{
			char c[] = "123521";
			Assert::AreEqual(false, Palindrom(c));
		}

		TEST_METHOD(cesare_test)
		{
			string c = "����";
			string a = "����";
			Assert::AreEqual(a, Cesare(c, 3));
		}

		TEST_METHOD(round_test)
		{
			double c = 3.1415;
			Assert::AreEqual(3.14, Round(c, 2));
		}
		TEST_METHOD(Testelem)
		{
			int a = 5;
			AmountOfElements(7, a);
			Assert::AreEqual(7, a);
		}
		TEST_METHOD(Testelem2)
		{
			int a = 3;
			AmountOfElements(3, a);
			Assert::AreEqual(3, a);
		}
		TEST_METHOD(Testglasnie)
		{
			int gl = 0;
			AmountOfVowels("��������", 8, "��������", gl);
			Assert::AreEqual(4, gl);
		}
		TEST_METHOD(Testglasnie2)
		{
			int gl = 0;
			AmountOfVowels("����������", 10, "��������", gl);
			Assert::AreEqual(2, gl);
		}
		TEST_METHOD(Testsoglasnie)
		{
			int sg = 0;
			AmountOf�onsonants("������", 6, "��������������������", sg);
			Assert::AreEqual(4, sg);
		}
		TEST_METHOD(Testsoglasnie2)
		{
			int sg = 0;
			AmountOf�onsonants("��������", 8, "��������������������", sg);
			Assert::AreEqual(5, sg);
		}

		TEST_METHOD(Testchange)
		{
			string s = "����";
			string s2 = "����";
			s = ChangeOfSymbol(s, '�', '�', 4);
			Assert::AreEqual(s2, s);
		}

		TEST_METHOD(Testnochange)
		{
			string s = "dfhvfghb";
			string s2 = "������ �� �������� ������ ������.";
			s = ChangeOfSymbol(s, 'i', 'v', 8);
			Assert::AreEqual(s2, s);
		}

		TEST_METHOD(Mirror_test)
		{
			string s = "���������";
			string s3 = "���������";
			string s2 = Mirror(s, 9);
			Assert::AreEqual(s3, s2);
		}

		TEST_METHOD(AmountOfSymbol_test)
		{
			string s = "���������";
			int k = AmountOfSymbol(s, '�', 9);
			Assert::AreEqual(3, k);
		}
	};
}