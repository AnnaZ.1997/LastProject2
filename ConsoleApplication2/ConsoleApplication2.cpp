#include "stdafx.h"
#include <string>
#include <iostream>
#include <Windows.h>
#include <algorithm> 
#include <iterator> 
#include <cctype> 
#include"conio.h"

using namespace std;

bool Palindrom(string str);
bool AmountOfElements(int summ, int & elem);
bool AmountOfVowels(string str, int summ, char* glas, int & gl);
bool AmountOf�onsonants(string str, int summ, char* sog, int & sg);
double Round(double n, int k);
string Cesare(string s, int key);
string ChangeOfSymbol(string s, char n, char z, int summ);
string Mirror(string s, int summ);
int AmountOfSymbol(string s, char c, int summ);

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	string str;
	cout << "������� ������: ";
	cin >> str;

	int summ;
	summ = str.length();

	char n, z, c;

	int menu;
	do
	{
		cout << endl << "������� ������: " << str << endl;
		cout << "����." << endl;
		cout << "1 - ��������, �������� �� ������ �����������." << endl;
		cout << "2 - ����� ��������� ������." << endl;
		cout << "3 - ���������� ������� ���� � ������." << endl;
		cout << "4 - ���������� ��������� ���� � ������." << endl;
		cout << "5 - ���������� �����." << endl;
		cout << "6 - ���������� ������� ������." << endl;
		cout << "7 - ������ �������." << endl;
		cout << "8 - ����� ������ � �������� �������." << endl;
		cout << "9 - ���������� �������� �������� � ������." << endl;
		cout << "10 - �������� ������." << endl;
		cout << "~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
		cout << "0 - �����." << endl << endl;
		cout << "������� ����� ����: ";
		while (!(cin >> menu) || menu < 0 || menu > 10)
		{
			cout << "�������� ��������." << endl;
			cout << "������� ����� ����: ";
			cin.clear();
			if (cin.get() != '\n')
			{
				continue;
			}
		}
		if (menu == 1)
		{
			Palindrom(str);
			_getch();
			system("cls");
		}

		else if (menu == 2)
		{
			int summ;
			int elem;
			summ = str.length();
			AmountOfElements(summ, elem);
			cout << "���������� ���� � ������ = " << elem << endl;
			_getch();
			system("cls");
		}

		else if (menu == 3)
		{
			char glas[] = "���������";
			int gl = 0;
			summ = str.length();
			AmountOfVowels(str, summ, glas, gl);
			cout << "���������� ������� = " << gl << endl;
			_getch();
			system("cls");
		}

		else if (menu == 4)
		{
			char sog[] = "��������������������";
			int sg = 0;
			AmountOf�onsonants(str, summ, sog, sg);
			cout << "���������� ��������� � ������ = " << sg;
			_getch();
			system("cls");
		}

		else if (menu == 5)
		{
			int k;
			double n;

			cout << endl << "������� �����: ";
			cin >> n;
			cout << "������� ���������� ������: ";
			cin >> k;
			Round(n, k);
			_getch();
			system("cls");
		}

		else if (menu == 6)
		{
			int key;
			cout << "������� ����: ";
			cin >> key;
			Cesare(str, key);
			_getch();
			system("cls");
		}

		else if (menu == 7)
		{
			cout << "������� ������, ������� ����� ��������: ";
			cin >> n;
			cout << "������ ��� ������: ";
			cin >> z;
			ChangeOfSymbol(str, n, z, summ);
			_getch();
			system("cls");
		}

		else if (menu == 8)
		{
			cout << Mirror(str, summ);
			_getch();
			system("cls");
		}

		else if (menu == 9)
		{
			cout << "������� ������: ";
			cin >> c;
			cout << AmountOfSymbol(str, c, summ);
			_getch();
			system("cls");
		}

		else if (menu == 10)
		{
			cout << "������� ������: ";
			cin >> str;
			_getch();
			system("cls");
		}

		else if (menu == 0)
			break;

	} while (menu != 0);

	return 0;
}

bool Palindrom(string str)
{
	setlocale(LC_ALL, "rus");
	bool poli = true;
	while (poli == true)
	{
		if ((str.length() % 2) == 0)
		{
			for (unsigned i = 0; i < str.length() / 2; i++)
			{
				if (str.at(i) != str.at(str.length() - i - 1))
				{
					poli = false;
					break;
				}
			}break;

		}
		else
		{
			for (unsigned i = 0; i < (str.length() - 1) / 2; i++)
			{
				if ((str.at(i) != str.at(str.length() - i - 1)))
				{
					poli = false;
					break;
				}
			}break;


		}
	}
	cout << poli << endl;
	return(poli);
}

bool AmountOfElements(int summ, int & elem)
{
	elem = summ;
	return elem;
}

bool AmountOfVowels(string str, int summ, char* glas, int & gl)
{
	for (int i = 0; i < summ; i++)
		for (int j = 0; j < strlen(glas); j++)
			if (str[i] == glas[j])
				gl++;
	return gl;
}

bool AmountOf�onsonants(string str, int summ, char* sog, int & sg)
{
	for (int i = 0; i < summ; i++)
		for (int j = 0; j < strlen(sog); j++)
			if (str.at(i) == sog[j])
				sg++;
	return sg;
}

double Round(double n, int k)
{
	double res;
	res = round(n * pow(10, k)) / pow(10, k);
	cout << res << endl;
	return(res);
}

string Cesare(string s, int key)
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	string res;
	transform(s.begin(), s.end(), back_inserter(res), tolower);
	for (int i = 0; i < (res.length()); i++)
	{
		res.at(i) = res.at(i) + (key + 33) % 33;
	}
	cout << res << endl;
	return res;
}

string ChangeOfSymbol(string s, char n, char z, int summ)
{
	string s2;
	s2 = s;
	for (int i = 0; i < summ; i++)
	{
		if (s.at(i) == n)
		{
			s.at(i) = z;
		}
		if (s == s2)
		{
			s = "������ �� �������� ������ ������.";
		}
	}
	cout << s;
	return s;
}

string Mirror(string str, int summ)
{
	string s2;
	for (int i = summ - 1; i > -1; i--)
		s2 = s2 + str[i];
	return s2;
}

int AmountOfSymbol(string s, char c, int summ)
{
	int k = 0;
	for (int i = 0; i < summ; i++)
	{
		if (s.at(i) == c)
			k++;
	}
	return k;
}